﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndScene : MonoBehaviour
{
    public GameObject EndPanelCanvas;

    public void enableEndPanelCanvas()
    {
        EndPanelCanvas.SetActive(true);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public FixedJoystick fixedJoystick;
    private bool jump = false;

    bool facingRight = true;

    [SerializeField] float maxSpeed;
    [SerializeField] float jumpForce;

    [SerializeField] bool airControl = true;
    [SerializeField] LayerMask whatIsGround;

    public Transform groundCheck;
    float groundRadius = 0.5f;
    bool grounded = false;
    Animator anim;

    public float jumpTime = 0.1f;
    public float jumpTimer;

    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponent<Animator>();
        jumpTimer = 0;
    }

    // Update is called once per frame
    void Update()
    {

        Vector3 direction = Vector3.forward * fixedJoystick.Vertical + Vector3.right * fixedJoystick.Horizontal;
        
        if(fixedJoystick.Vertical > 0.3)
        {
            jump = true;
        }

        float h = fixedJoystick.Horizontal;
        Move(h, jump);
        jump = false;


        Rigidbody2D rigidbody = GetComponent<Rigidbody2D>();
        anim.SetInteger("Jump", (int)rigidbody.velocity.y);
    }

    private void FixedUpdate()
    {
        grounded = Physics2D.OverlapCircle(groundCheck.position, groundRadius, whatIsGround);
        anim.SetBool("Ground", grounded);
    }

    void Move(float move, bool jump)
    {
        if(grounded || airControl)
        {
            anim.SetFloat("Speed", Mathf.Abs(move));

            GetComponent<Rigidbody2D>().velocity = new Vector2(move * maxSpeed, GetComponent<Rigidbody2D>().velocity.y);

            if (move > 0 && !facingRight)
                Flip();
            else if (move < 0 && facingRight)
                Flip();
        }

        jumpTimer += Time.deltaTime;
        if (jumpTimer > jumpTime)
        {
            jumpTimer = 0;
            if (grounded && jump)
            {
                Rigidbody2D rigidbody = GetComponent<Rigidbody2D>();
                rigidbody.AddForce(new Vector2(0f, jumpForce));
            }
        }
        
    }

    void Flip()
    {
        facingRight = !facingRight;

        Vector3 theScale = transform.localScale;
        theScale.x *= -1;
        transform.localScale = theScale;
    }
}
